# SaveSystem

HBF的游戏存档保存系统，使用自[Easy Save - The Complete Save Data & Serializer System | 实用工具 工具 | Unity Asset Store](https://assetstore.unity.com/packages/tools/utilities/easy-save-the-complete-save-data-serializer-system-768)

## 如何使用

强烈建议直接下载zip后解压到项目中，虽然可以以包的形式添加，但是无法打开设置界面等

1. 复制git地址：`https://gitlab.com/hbfpt/Unity/Plugin/savesystem.git`
2. 打开工程，点击窗口->包管理器->加号->添加来自git URL的包

![image.png](https://s2.loli.net/2023/04/08/b1rn9XVG6diJq7v.png)

3. 输入复制的git地址
4. 等待下载完成即可！